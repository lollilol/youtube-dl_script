@echo off

REM set location
set mp4_location=%USERPROFILE%\Videos
set mp3_location=%USERPROFILE%\Music

REM set format (default: "%%(title)s-%%(uploader)s.%%(ext)s")
REM available tags: https://github.com/rg3/youtube-dl#output-template

REM add Playlist Numbers to Format: %%(playlist_index)s. 
REM final (you can copy that): "%%(playlist_index)s. %%(title)s - %%(uploader)s.%%(ext)s"

set format=%%(title)s - %%(uploader)s.%%(ext)s


REM no necessary changes beyond this line

set /p url=Youtube URL: 
set /p choice=MP4 or MP3 (mp3/mp4): 
if '%choice%' == 'mp4' goto mp4
if '%choice%' == 'mp3' goto mp3
Goto Ende
:mp4
youtube-dl.exe -i -o "%mp4_location%\%format%" -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]" %url%
goto Ende
:mp3
youtube-dl.exe -i -o "%mp3_location%\%format%" -x --audio-format mp3 %url%

:Ende
pause
