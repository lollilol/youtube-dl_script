# Batch script to download youtube videos
A customizable, little batch script to download youtube videos or playlists.

Features:
+ Download Youtube videos/playlists to a customizable path and customizable video file name/format.
+ it should download the videos in your videos/music folder, if not, edit the script and change the value.

## Downloads

[.exe Installer/automatic extractor](https://github.com/lollilol/youtube-dl_script/releases/download/v1.0/Youtube-DL_Script_Installer.exe) (i would recommend to extract it to the desktop)

[.zip Archive](https://github.com/lollilol/youtube-dl_script/releases/download/v1.0/Youtube-DL.zip)

dont be surprised, ive made the ffmpeg and youtube-dl.exe files invisible, you only can see Youtube-DL-bat unless you enable this:
![how to enable it](https://image.prntscr.com/image/xvUzohQwTwCFQO2heL_piQ.png)

[Only script](https://github.com/lollilol/youtube-dl_script/releases/download/v1.0/Youtube_DL.bat)

[Raw script](https://raw.githubusercontent.com/lollilol/youtube-dl_script/master/Youtube-DL.bat)

## How to install:

+ Just [download](#downloads) the script, edit it, and then start/use it!

## Required/Dependencies
+ [youtube-dl](https://youtube-dl.org/downloads/latest/youtube-dl.exe) named "youtube-dl.exe" in **C:\Windows\System32** or in the current folder, from where you executing the script.
